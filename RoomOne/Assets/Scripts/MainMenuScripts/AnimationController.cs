using UnityEngine;

public class AnimationController : MonoBehaviour {
    public Transform player;
    public Transform monster;
    public float speed = 0.1f;
    public Transform leftBorder, rightBorder;


    private void Start() {        
        PlayPlayerAnimation();
        PlayMonsterAnimation(); 
        BackgroundAudioManager.instance.sound = true;
    }

    private void Update() {
        Move( player );
        Move( monster );
    }

    private void PlayPlayerAnimation() {
        player.GetComponent<Animator>().SetBool( TagsManager.WALK_PARAM, true );
    }

    private void PlayMonsterAnimation() {
        monster.GetComponent<Animator>().SetTrigger( TagsManager.HUNT_PARAM );
    }

    private void Move( Transform gameObject ) {
        Vector3 targetPosition = gameObject.position;
        targetPosition.x = gameObject.localScale.x > 0 ? rightBorder.position.x : leftBorder.position.x;
        gameObject.position = Vector3.MoveTowards( gameObject.position, targetPosition, speed );

        if ( Mathf.Abs( gameObject.position.x - targetPosition.x ) < 0.1f ) {
            ChangeDirection( gameObject );
        }
    }

    private void ChangeDirection( Transform gameObject ) {
        Vector3 tempScale = gameObject.localScale;
        tempScale.x *= -1f;
        gameObject.localScale = tempScale;
    }
}