using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour {
    public GameObject settingsPanel;
    public GameObject helpGuidePanel;
    private GameMode gameMode = GameMode.Help;
    private int currentLanguageIndex = 0;


    public void PlayGame() {
        SaveSettings();
        LoadIntroCutscene();
    }

    public void OpenSettings() {
        helpGuidePanel.SetActive( false );
        settingsPanel.SetActive( !settingsPanel.activeSelf );
    }

    public void SetHelpGameMode( bool isOn ) {
        gameMode = isOn ? GameMode.Help : GameMode.General;
    }

    public void OffBackSound( bool isOff ) {
        BackgroundAudioManager.instance.sound = !isOff;
    }

    public void SetNextLanguage() {
        currentLanguageIndex = ( currentLanguageIndex + 1 ) % LocalizationSystem.avialableLanguages.Length;
        LocalizationUI.SetCurrentLanguage( LocalizationSystem.avialableLanguages[ currentLanguageIndex ] );
    }

    public void SetPrevLanguage() {
        currentLanguageIndex = ( currentLanguageIndex - 1 + LocalizationSystem.avialableLanguages.Length ) % LocalizationSystem.avialableLanguages.Length;
        LocalizationUI.SetCurrentLanguage( LocalizationSystem.avialableLanguages[ currentLanguageIndex ] );
    }

    private void SaveSettings() {
        PlayerPrefs.SetInt( TagsManager.GAMEMODE_KEY, (int)gameMode );
        PlayerPrefs.SetInt( TagsManager.LOCALIZATION_KEY, (int)LocalizationSystem.currentLanguage );
        PlayerPrefs.Save();
    }

    public void ShowHelpGuide() {
        settingsPanel.SetActive( false );
        helpGuidePanel.SetActive( !helpGuidePanel.activeSelf );
    }
    
    public void CloseGame() {
        Application.Quit();
    }

    private void LoadIntroCutscene() {
        SceneManager.LoadScene( TagsManager.INTRO_CUTSCENE );
    }
}