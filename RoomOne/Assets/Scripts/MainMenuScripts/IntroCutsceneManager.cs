using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroCutsceneManager : MonoBehaviour {

    private void Update() {
        if ( Input.GetKeyDown( KeyCode.Space ) ) {
            LoadLevel();
        }
    }

    public void LoadLevel() {
        if ( (GameMode)PlayerPrefs.GetInt( TagsManager.GAMEMODE_KEY ) == GameMode.Help ) {
            SceneManager.LoadScene( TagsManager.ROOM_ONE_HELP_MODE_SCENE );
        }
        else {
            SceneManager.LoadScene( TagsManager.ROOM_ONE_SCENE );
        }
    }
}