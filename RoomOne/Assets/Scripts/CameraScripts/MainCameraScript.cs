using UnityEngine;

public class MainCameraScript : MonoBehaviour {
    public static MainCameraScript instance;
    private float defOrthograficSize;
    
    private Vector3 velocity;
    private float followingSpeed = 0.1f;

    private Transform pursuedObject;
    private bool changePosY = false;
    private Transform player;

    public Transform leftBottomBorder, rightUpBorder;

    private void Awake() {
        MakeInstance();
        defOrthograficSize = Camera.main.orthographicSize;
    }

    private void Start() {
        player = GameObject.FindGameObjectWithTag( TagsManager.PLAYER_TAG ).transform;
        pursuedObject = player;
    }

    private void FixedUpdate() {
        FollowObject();
    }

    private void MakeInstance() {
        if ( instance == null ) {
            instance = this;
        }
        else {
            Destroy( instance );
        }
    }

    protected void FollowObject() {
        transform.position = Vector3.SmoothDamp(transform.position, GetNewPosition(), ref velocity, followingSpeed );
    }

    private Vector3 GetNewPosition() {
        Vector3 target = transform.position;
        target.x = Mathf.Max( leftBottomBorder.position.x + camSize.x / 2f, Mathf.Min( pursuedObject.position.x, rightUpBorder.position.x - camSize.x / 2f ));
        
        if ( changePosY ) {
            target.y = Mathf.Max( leftBottomBorder.position.y + camSize.y / 2f, Mathf.Min( pursuedObject.position.y, rightUpBorder.position.y - camSize.y / 2f));
        }
        else {
            target.y = 0f;
        }

        return target;
    }

    private Vector2 camSize {
        get {
            return new Vector2( 2f * Camera.main.aspect * Camera.main.orthographicSize, 2f * Camera.main.orthographicSize );
        }
    }

    public void FixOnItem( Transform target ) {
        pursuedObject = target;
        ZoomItem( target );        
        changePosY = true;
    }

    private void ZoomItem( Transform target ) {
        Vector2 size = target.GetComponent<SpriteRenderer>().bounds.size;
        float scale = Mathf.Max( size.x / camSize.x, size.y / camSize.y );
        Camera.main.orthographicSize *= scale;
    }

    public void Reset() {
        pursuedObject = player;
        Camera.main.orthographicSize = defOrthograficSize;
        changePosY = false;
        transform.position = GetNewPosition();
    }
}