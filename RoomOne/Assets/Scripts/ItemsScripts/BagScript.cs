using UnityEngine;

public class BagScript : ItemController {
    public int bagSize = 10;
    
    override protected void Activate() {
        base.Activate();
        GameObject.FindGameObjectWithTag( TagsManager.PLAYER_TAG ).GetComponent<ItemsHandler>().FindBag( bagSize );
        gameObject.SetActive( false );
    }    
}