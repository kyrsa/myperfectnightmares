using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class FlashlightScript : UsableItemController {
    public GameObject myLight;
    private bool isOn;

    public float intensity {
        set => myLight.GetComponent<Light2D>().intensity = value;
        get => isOn ? myLight.GetComponent<Light2D>().intensity : 0f;
    }

    override public void Use() {
        base.Use();
        myLight.SetActive( !myLight.activeSelf );
        isOn = myLight.activeSelf;
    }
}