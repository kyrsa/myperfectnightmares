﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : MonoBehaviour{
    protected BoxCollider2D myCollider;
    protected bool isColliding = false;

    protected SpriteRenderer spriteRenderer;
    public ItemController blocker;   
    protected bool used = false;

    public GameObject hintPrefab;
    public Sprite hintSprite;
    protected HintController hint;
    public Vector3 hintOffset = new Vector3(0f, 1f, 0f);


    virtual protected void Awake() {
        myCollider = GetComponent<BoxCollider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        visible = blocker == null;
    }

    virtual protected void Start() {
        CreateHint();
    }

    private void CreateHint() {
        hint = Instantiate( hintPrefab ).GetComponent<HintController>();
        hint.Tune( this, hintSprite, hintOffset );
    }

    virtual protected void Update() {
        if ( !visible && !isBlocked ) {
            visible = true;
        }

        if ( GameplayController.instance.gameMode == GameMode.Help ) {
            SwitchHintState();
        }

        if ( isSelected) {
            Activate();
        }
    }

    virtual protected void OnTriggerEnter2D(Collider2D other) {
        if ( other.tag == TagsManager.PLAYER_TAG ) {
            isColliding = true;
        }
    }

    virtual protected void OnTriggerExit2D(Collider2D other) {
        if ( other.tag == TagsManager.PLAYER_TAG ) {
            isColliding = false;
        }
    }

    virtual public Sprite img {
        protected set => spriteRenderer.sprite = value;
        get => spriteRenderer.sprite;
    }

    public string layerName {
        get => spriteRenderer.sortingLayerName;
    }

    public int layerOrder {
        get => spriteRenderer.sortingOrder;
    }

    public Vector2 size {
        get => img.bounds.size;
    }

    virtual public bool visible {
        set => spriteRenderer.enabled = value;
        get => spriteRenderer.enabled;
    }

    public bool isUsed {
        protected set => used = value;
        get => used;
    }

    protected bool isInteractable {
        get => !GameplayController.instance.isFreezeMode && !isBlocked && !isUsed && isColliding;
    }

    protected bool isSelected {
        get => isInteractable && isClicked;
    }

    protected bool isBlocked {
        get => !blocker?.isUsed ?? false;
    }

    protected bool isClicked {
        get => isMouseOver && Input.GetMouseButtonDown(0);
    }

    protected bool isMouseOver {
        get => myCollider.OverlapPoint( Camera.main.ScreenToWorldPoint( Input.mousePosition ) );
    }

    protected void SwitchHintState() {
        if ( isInteractable ) {
            hint.visible = true;
            hint.highlighted = isMouseOver;
        }
        else {
            hint.visible = false;
        }       
    }

    virtual protected void Activate() {
        AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.itemSelectedClip );
    }
}
