using UnityEngine;

public class ZoomItemController : ItemController {
    public GameObject panel;

    override protected void Activate() {
        base.Activate();
        Camera.main.GetComponent<MainCameraScript>().FixOnItem( transform );
        panel.SetActive( true );
    }

    virtual public void Exit() {
        panel.SetActive( false );
        Camera.main.GetComponent<MainCameraScript>().Reset();
    }
}