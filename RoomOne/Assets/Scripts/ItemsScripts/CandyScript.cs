using UnityEngine;

public class CandyScript : UsableItemController {
    private bool onTree;


    override protected void Start() {
        base.Start();
        onTree = true;
        hint.transform.position = myCollider.bounds.center;
    }

    override protected void Activate() {
        base.Activate();

        if ( onTree ) {
            onTree = false;
            myCollider.offset = Vector2.zero;
            myCollider.size = img.bounds.size;
            hint.transform.localPosition = new Vector3(hintOffset.x, hintOffset.y, 0f);
        }
    }

    public bool isOnFloor {
        get => gameObject.activeSelf && !onTree && !isGrapped;
    }


    public void Eat() {
        isUsed = true;
        gameObject.SetActive( false );
    }
}