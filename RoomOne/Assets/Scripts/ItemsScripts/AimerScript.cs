using UnityEngine;

public class AimerScript : MonoBehaviour {
    private CircleCollider2D myCollider;
    private Vector2 imageSize;

    public float noise = 0.1f;
    private Vector3 velocity;
    public float speed = 0.1f;

    private GameObject[] targetItems;
    private GameObject collidingItem;

    private float soundOnDistance = 5f;
    private float soundMaxVolume = 1f;
    public delegate void ItemCaughtDelegate( GameObject caughtItem );
    public event ItemCaughtDelegate itemCaughtEvent;

    private void Awake() {
        myCollider = GetComponent<CircleCollider2D>();
        imageSize = GetComponent<SpriteRenderer>().sprite.bounds.size;
    }

    private void Start() {
        targetItems = GameObject.FindGameObjectsWithTag( TagsManager.AIMER_TARGET );
    }

    private void OnEnable() {
        if ( GameplayController.instance.RequestFreezeMode( gameObject ) ) {
            MainCameraScript.instance.FixOnItem( transform );
            transform.rotation = Quaternion.identity;
            AudioManager.instance.aimerAudioSource.Play();
        }
        else {
            gameObject.SetActive( false );
        }
    }

    private void Update() {
        ChangeAudioHintVolume();

        if ( Input.GetKeyDown( KeyCode.Return ) || Input.GetMouseButtonDown(0) ) {
            TryCatch();
        }

        if ( Input.GetKeyDown( KeyCode.E ) ) {
            Close();
        }
    }

    private void FixedUpdate() {
        Move();
    }

    private void ChangeAudioHintVolume() {
        float distance = FindDistanceToNearestItem();
        AudioManager.instance.aimerAudioSource.volume = distance < soundOnDistance ? soundMaxVolume * (soundOnDistance - distance) / soundOnDistance : 0f;
    }

    private float FindDistanceToNearestItem() {
        float minDistance = 100000f;
        float distance;

        foreach ( var target in targetItems ) {
            distance = Vector2.Distance( transform.position, target.transform.position );
            
            if ( distance < minDistance ) {
                minDistance = distance;
            }
        }

        return minDistance;
    }

    private void Move() {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        float randShiftX = Random.Range(-noise, noise);
        float randShiftY = Random.Range(-noise, noise);

        float newPositionX = Mathf.Max( MainCameraScript.instance.leftBottomBorder.position.x + imageSize.x / 2f, Mathf.Min( transform.position.x + h + randShiftX, MainCameraScript.instance.rightUpBorder.position.x - imageSize.x / 2f ) );
        float newPositionY = Mathf.Max( MainCameraScript.instance.leftBottomBorder.position.y + imageSize.y / 2f, Mathf.Min( transform.position.y + v + randShiftY, MainCameraScript.instance.rightUpBorder.position.y - imageSize.y / 2f ) );

        Vector3 newPosition = new Vector3( newPositionX, newPositionY, 0f);
        transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref velocity, speed );
    }


    private void OnTriggerEnter2D(Collider2D other) {
        if ( other.tag == TagsManager.AIMER_TARGET ) {
            collidingItem = other.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if ( collidingItem != null && other.tag == collidingItem.tag ) {
            collidingItem = null;
        }
    }

    private void TryCatch() {       
        if ( collidingItem != null ) {
            Catch();
            Close();
        }
        else {
            AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.itemLockedClip );
        }
    }

    private void Catch() {
        AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.itemSelectedClip );
        GameplayController.instance.RequestUnfreezeMode( gameObject );
        itemCaughtEvent?.Invoke( collidingItem );
    }

    private void Close() {
        GameplayController.instance.RequestUnfreezeMode( gameObject );
        AudioManager.instance.aimerAudioSource.Stop();
        MainCameraScript.instance.Reset();
        gameObject.SetActive( false );
    }
}