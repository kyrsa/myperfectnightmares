using UnityEngine;

public class ShelfScript : ZoomItemController {
    public GameObject myLight;
    
    public bool isActivated {
        get => myLight.activeSelf;
    } 

    override protected void Activate() {
        if ( GameplayController.instance.RequestFreezeMode( gameObject ) ) {
            base.Activate();
            myLight.SetActive( true );
        }
    } 

    override public void Exit() {
        GameplayController.instance.RequestUnfreezeMode( gameObject );
        myLight.SetActive( false );
        base.Exit();
    }
}