using UnityEngine;
using System.Collections;

public class ExplorableItemController : ItemController {
    protected Animator animator;

    override protected void Awake() {
        base.Awake();
        animator = GetComponent<Animator>();
    }

    override protected void Activate() {     
        base.Activate();
        animator.SetTrigger( TagsManager.ACTIVATE_PARAM );  
    }

    public void SetUninteractive() {
        isUsed = true;
    }
}