using UnityEngine;

public class BombonScript : ExplorableItemController {
    override protected void Activate() {}

    private void OnParticleCollision(GameObject other) {
        if ( other.tag == TagsManager.WATER_TAG ) {
            base.Activate();
        }
    }
}