using UnityEngine;

public class BottleScript : UsableItemController {
    private Animator animator;
    public ParticleSystem dropFX;

    override protected void Awake() {
        base.Awake();
        animator = GetComponent<Animator>();
    }

    override public void Use() {
        if ( GameplayController.instance.RequestFreezeMode( gameObject ) ) {
            isUsed = true;
            RunPouringAnimation();        
        }        
    }

    private void RunPouringAnimation() {
        transform.localRotation = Quaternion.identity;
        animator.SetTrigger( TagsManager.ACTIVATE_PARAM );
        dropFX.Play();
        AudioManager.instance.waterAudioSource.Play();
    }

    public void Deactivate() {
        GameplayController.instance.RequestUnfreezeMode( gameObject );
        AudioManager.instance.waterAudioSource.Stop();
        gameObject.SetActive(false);
    }    
}