using UnityEngine;

public class BookScript : ItemController {
    public GameObject bookPanelIcon;

    override protected void Awake() {
        base.Awake();
        visible = true;
    }

    override protected void Update() {
        base.Update();

        if ( canSelected && isClicked ) {
            Activate();
        }
    }

    private bool canSelected {
        get => ((ShelfScript)blocker).isActivated;
    }

    override protected void Activate() {
        base.Activate();
        ItemsPanel.instance.ActivateBooksIcon();
        bookPanelIcon.SetActive( true );
        gameObject.SetActive( false );
    }
}