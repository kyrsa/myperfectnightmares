using UnityEngine;

public class PictureStarsScript : ZoomItemController {
    private Animator animator;
    public GameObject frontLight;
    public StarScript[] stars;

    override protected void Awake() {
        base.Awake();
        animator = GetComponent<Animator>();
    }

    override protected void Start() {
        base.Start();
        SetStarsActive( false );
    }

    override protected void Activate() {
        if ( GameplayController.instance.RequestFreezeMode( gameObject ) ) {
            base.Activate();
            SetStarsActive( true );
        }
    }

    private void SetStarsActive( bool isActive ) {
        foreach ( var star in stars ) {
            star.gameObject.SetActive( isActive );
        }
    }

    public void Reset() {
        foreach ( var star in stars ) {
            star.selected = false;
        }
    }

    public void Check() {
        foreach ( var star in stars ) {
            if ( star.correctState != star.selected ) {
                AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.itemLockedClip );
                return;
            }
        }

        Reset();
        Exit();

        animator.SetTrigger( TagsManager.ACTIVATE_PARAM );
        isUsed = true;
        AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.itemUsedClip );
    }

    override public void Exit() {
        GameplayController.instance.RequestUnfreezeMode( gameObject );
        SetStarsActive( false );
        base.Exit();
    }
}