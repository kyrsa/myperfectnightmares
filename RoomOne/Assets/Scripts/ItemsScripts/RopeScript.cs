using UnityEngine;
using System.Collections;

public class RopeScript : UsableItemController {
    public Sprite ropeInHandImage;
    public Sprite freeRopeImage;
    public Transform linkPoint;
    private float ropeLength;
    
    public AimerScript aimer;

    private Transform caughtItemHook;
    private Vector3 caughtItemHookOffset;
    public float draggingStopDistance = 3f;
    public float draggingSpeed = 0.1f;
    private Vector3 draggingVelocity;

    override protected void Awake() {
        base.Awake();
        
        ropeLength = linkPoint.GetComponentInChildren<SpriteRenderer>().sprite.bounds.size.x;
        linkPoint.gameObject.SetActive( false );

        aimer.itemCaughtEvent += CatchItem;
    }

    override protected void Activate() {
        img = ropeInHandImage;
        base.Activate();
    }

    override protected void TuneTransformToFitHand( Transform parent ) {
        base.TuneTransformToFitHand( parent );
        FixVerticalPosition();        
    }

    protected void FixVerticalPosition() {
        Vector3 tempPosititon = transform.localPosition;
        tempPosititon.y -= img.bounds.size.y / 2f;
        transform.localPosition = tempPosititon;
    }

    override protected void DoBeforeDroppingStaff() {
        base.DoBeforeDroppingStaff();
        img = freeRopeImage;
    }   

    override public void Use() {
        if ( !GameplayController.instance.isFreezeMode ) {
            base.Use();
            aimer.transform.position = transform.position;
            aimer.gameObject.SetActive( true );
        }
    }

    public void CatchItem( GameObject hook ) {
        Debug.Log(CanDrag(hook));

        if ( CanDrag( hook ) && GameplayController.instance.RequestFreezeMode( gameObject ) ) {
            SetCaughtItemInfo( hook );
            StartCoroutine( Drag() );
        }
    }

    private bool CanDrag( GameObject hook ) {
        BoxCollider2D caughtItem = hook.transform.GetChild(0).GetComponent<BoxCollider2D>();
        BoxCollider2D player =  GameObject.FindGameObjectWithTag( TagsManager.PLAYER_TAG ).GetComponent<BoxCollider2D>();
        return !caughtItem.IsTouching( player );
    }

    private void SetCaughtItemInfo( GameObject item ) {
        caughtItemHook = item.transform;
        BoxCollider2D[] caughtItemHooks = caughtItemHook.GetComponents<BoxCollider2D>();
        
        if ( caughtItemHook.transform.position.x > linkPoint.position.x ) {
            caughtItemHookOffset = caughtItemHooks[0].offset;
        }
        else {
            caughtItemHookOffset = caughtItemHooks[caughtItemHooks.Length - 1].offset;
        }
    }

    protected IEnumerator Drag( ) {
        linkPoint.gameObject.SetActive( true );
        Vector3 targetPosition = caughtItemHook.transform.position;
        targetPosition.x = linkPoint.position.x;

        while ( Mathf.Abs( targetPosition.x - caughtItemHook.transform.position.x ) > draggingStopDistance ) {
            caughtItemHook.transform.position = Vector3.SmoothDamp( caughtItemHook.transform.position, targetPosition, ref draggingVelocity, draggingSpeed );
            ResizeMiddlePart();
            yield return new WaitForFixedUpdate();            
        }

        linkPoint.gameObject.SetActive( false );
        GameplayController.instance.RequestUnfreezeMode( gameObject );
    }

    protected void ResizeMiddlePart() {
        RotateMiddlePart();
        ChangeMiddlePartLength();
    }

    protected void RotateMiddlePart() {
        Vector3 currentRotation = linkPoint.rotation.eulerAngles;
        currentRotation.z = GetAngleBetweenRopeAndItem();
        linkPoint.rotation = Quaternion.Euler( currentRotation );
    }

    protected float GetAngleBetweenRopeAndItem() {   
        float angle = Mathf.Atan2(linkPoint.position.y - ( caughtItemHook.position.y + caughtItemHookOffset.y ), linkPoint.position.x - ( caughtItemHook.position.x + caughtItemHookOffset.x ) ) * 180f / Mathf.PI;
        return angle;
    }

    protected void ChangeMiddlePartLength() {
        float scaleFixer = -Mathf.Sign( GameObject.FindGameObjectWithTag( TagsManager.PLAYER_TAG ).transform.localScale.x );

        Vector3 currentScale = linkPoint.localScale;
        linkPoint.localScale = new Vector3( scaleFixer * Vector2.Distance( linkPoint.position, caughtItemHook.position + caughtItemHookOffset ) / ropeLength, currentScale.y, currentScale.z );
    }
}