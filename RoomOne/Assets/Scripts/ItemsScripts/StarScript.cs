using UnityEngine;

public class StarScript : MonoBehaviour {
    CircleCollider2D myCollider;
    Animator animator;
    public bool correctState = false;
    bool isSelected = false;

    private void Awake() {
        myCollider = GetComponent<CircleCollider2D>();
        animator = GetComponent<Animator>();
    }

    private void Update() {
        if ( isClicked ) {
            selected = !selected;
            AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.starSelectedClip );
        }
    }

    public bool selected {
        set {
            isSelected = value;
            animator.SetBool( TagsManager.ACTIVATE_PARAM, isSelected );
        }

        get => isSelected;
    }

    private bool isClicked {
        get => isMouseOver && Input.GetMouseButtonDown(0);
    }

    private bool isMouseOver {
        get {
            Vector3 mousePosWorld = Camera.main.ScreenToWorldPoint( Input.mousePosition );
            return myCollider.OverlapPoint( new Vector2( mousePosWorld.x, mousePosWorld.y )); 
        }
    }

    private void OnDisable() {
        selected = false;
    }
}