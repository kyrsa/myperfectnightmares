﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HintController : MonoBehaviour{
    Animator animator;
    SpriteRenderer spriteRenderer;
    ItemController ownerItem;

    private void Awake() {
        animator = GetComponent<Animator>();    
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;
    }

    public void Tune( ItemController item, Sprite sprite, Vector3 positionOffset ) {
        SetParent( item, positionOffset );
        TuneAppearance( sprite );        
    }

    private void SetParent( ItemController item, Vector3 positionOffset ) {
        ownerItem = item;
        transform.SetParent( ownerItem.transform, false );
        transform.localPosition = positionOffset;
    }

    private void TuneAppearance( Sprite sprite ) {
        img = sprite;
        transform.rotation = Quaternion.identity;
        TuneSpriteLayer();
    }

    private Sprite img {
        set => spriteRenderer.sprite = value;
    }

    private void TuneSpriteLayer() {
        layerName = ownerItem.layerName;
        layerOder = ownerItem.layerOrder + 10;
    }

    private string layerName {
        set => spriteRenderer.sortingLayerName = value;
    }

    private int layerOder {
        set => spriteRenderer.sortingOrder = value;
    }

    public bool visible {
        set { 
            spriteRenderer.enabled = value;
            transform.rotation = Quaternion.identity;
            TuneSpriteLayer();
        }
        
        get => spriteRenderer.enabled;
    }

    public bool highlighted {
        set => animator.SetBool( TagsManager.HIGHLIGHT_PARAM, value );
        get => animator.GetBool( TagsManager.HIGHLIGHT_PARAM );
    }
}
