using UnityEngine;

public class ToyBoxScript : ItemController {
    private Animator animator;
    [Range(0f, 1f)]
    public float openOpacity = 0.5f;
    private bool isPlayerInside = false;
    private PlayerMovements player;

    override protected void Awake() {
        base.Awake();
        animator = GetComponent<Animator>();
        visible = true;
    }

    override protected void Start() {
        base.Start();
        player = GameObject.FindGameObjectWithTag( TagsManager.PLAYER_TAG ).GetComponent<PlayerMovements>();
    }

    override protected void Update() {
        base.Update();

        if ( isInteractable ) {
            opacity = openOpacity;
        }
        else {
            opacity = 1f;
        }
    }

    private float opacity {
        set {
            Color color = spriteRenderer.color;
            color.a = value;
            spriteRenderer.color = color;
        }
    }

    public bool IsKeyInLock( GameObject key ) {
        return myCollider.OverlapPoint( key.transform.position );
    }

    public void Open() {
        animator.SetTrigger( TagsManager.ACTIVATE_PARAM );
    }

    override protected void Activate() {
        if ( isPlayerInside ) {
            GetOffPlayer();
        }
        else {
            HidePlayer();
        }
    }

    private void GetOffPlayer() {
        isPlayerInside = false;
        player.GetOffHidingPlace();
        spriteRenderer.sortingLayerName = TagsManager.BEHIND_LAYER;
    }

    private void HidePlayer() {
        isPlayerInside = true;
        player.Hide( transform.position.x );   
        spriteRenderer.sortingLayerName = TagsManager.PLAYER_LAYER; 
    }
}