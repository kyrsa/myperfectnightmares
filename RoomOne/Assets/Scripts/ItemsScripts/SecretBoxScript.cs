using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class SecretBoxScript : ItemController {
    private Animator animator;
    public GameObject myPanel;
    public int[] rightSymbolsIndices;
    public Sprite[] symbols;
    public Image[] codeIcons;
    private int[] currentSymbolsIndices;

    override protected void Awake() {
        base.Awake();
        animator = GetComponent<Animator>();
        SetCodeInitialState();
    }

    private void SetCodeInitialState() {
        currentSymbolsIndices = new int[rightSymbolsIndices.Length];

        for ( int i = 0; i < currentSymbolsIndices.Length; i++ ) {
            currentSymbolsIndices[i] = 0;
            codeIcons[i].sprite = symbols[0];
        }
    }

    public void SetPosition( Vector3 newPosition ) {
        newPosition.y = Mathf.Max( newPosition.y, GameplayBorders.instance.groundPosY + size.y / 2f);
        transform.position = newPosition;
    }

    override protected void Activate() {
        if ( GameplayController.instance.RequestFreezeMode( gameObject ) ) {
            base.Activate();
            myPanel.SetActive( true );
            visible = false;
        }
    }

    public void NextSymbol( int iconIndex ) {
        currentSymbolsIndices[iconIndex] += 1;
        currentSymbolsIndices[iconIndex] %= symbols.Length;
        codeIcons[iconIndex].sprite = symbols[currentSymbolsIndices[iconIndex]];
        AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.itemUsedClip );
    }

    public void PrevSymbol( int iconIndex ) {
        currentSymbolsIndices[iconIndex] += symbols.Length - 1;
        currentSymbolsIndices[iconIndex] %= symbols.Length;
        codeIcons[iconIndex].sprite = symbols[currentSymbolsIndices[iconIndex]];
        AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.itemUsedClip );
    }

    public void Check() {
        for ( int i = 0; i < rightSymbolsIndices.Length; i++ ) {
            if ( currentSymbolsIndices[i] != rightSymbolsIndices[i] ) {
                AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.itemLockedClip );
                return;
            }
        }

        animator.SetTrigger( TagsManager.ACTIVATE_PARAM );
        isUsed = true;
        AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.itemUsedClip );
        Exit();
    }

    public void Exit() {
        GameplayController.instance.RequestUnfreezeMode( gameObject );
        myPanel.SetActive( false );
        visible = true;
    }
}