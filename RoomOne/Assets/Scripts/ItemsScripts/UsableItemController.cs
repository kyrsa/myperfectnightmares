using UnityEngine;
using System.Collections;

public class UsableItemController : ItemController {
    [HideInInspector]
    public bool isGrapped = false;
    public float zRotInHand = 0f;
    public int weight = 1;
    private float droppingSpeed = 0.1f;
    private float fallingSpeed = 10f;
    protected ItemsHandler playerItemsHandler;

    override protected void Start() {
        base.Start();
        playerItemsHandler = GameObject.FindGameObjectWithTag( TagsManager.PLAYER_TAG ).GetComponent<ItemsHandler>();
    }

    override protected void Activate() {
        base.Activate();
        playerItemsHandler.Grap( this );
        isGrapped = true;
    }    

    public void HoldInHand( Transform handPoint, int layerOrder ) {
    	TuneSpriteRenderer( TagsManager.PLAYER_LAYER, layerOrder );
        TuneTransformToFitHand( handPoint );
    }

    virtual protected void TuneTransformToFitHand( Transform parent ) {
        transform.SetParent( parent, true );
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler( 0f, 0f, zRotInHand );
        Vector3 tempScale = transform.localScale;
    	transform.localScale = new Vector3( Mathf.Abs(tempScale.x), tempScale.y, 1f);
    }

    virtual protected void TuneSpriteRenderer( string name, int order ) {
        spriteRenderer.sortingLayerName = name;
    	spriteRenderer.sortingOrder = order;
    }

    public void Drop() {
        AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.itemDroppedClip );
        StartCoroutine( Dropping() );
    }

    protected IEnumerator Dropping() {
        DoBeforeDroppingStaff();
        Vector3 target = FindPositionOnGround();
        Vector3 velocity = new Vector3();

        while ( Mathf.Abs( transform.position.y - target.y ) > 1e-3 ) {
            transform.position = Vector3.SmoothDamp(transform.position, target, ref velocity, droppingSpeed, fallingSpeed );
            yield return new WaitForFixedUpdate();
        }

        DoAfterDroppingStaff();
    }

    virtual protected void DoBeforeDroppingStaff() {
        transform.SetParent( GameplayController.instance.itemsParent );
        transform.localRotation = Quaternion.identity;
    }

    virtual protected void DoAfterDroppingStaff() {
        TuneSpriteRenderer( TagsManager.BEHIND_LAYER, Random.Range(300, 500) );    
        isGrapped = false;
    }

    protected Vector3 FindPositionOnGround() {
        RaycastHit2D[] hits = Physics2D.RaycastAll( transform.position, Vector2.down, 100f, GameplayBorders.instance.groundLayer );

        foreach ( var hit in hits ) {
            if ( hit.distance != 0f ) {
                return new Vector3( hit.point.x, hit.point.y + size.y / 2f + 0.2f, 0f );
            }
        }
        
        return transform.position;
    }

    virtual public void Use() {
        AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.itemUsedClip );
    }
}