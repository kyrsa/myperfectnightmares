using UnityEngine;

public class MouseScript : UsableItemController {
    Animator animator;
    public float runTimeDuration = 3f;
    float runStartTime;
    public float runningSpeed = 0.3f;
    Vector3 velocity;

    override protected void Awake() {
        base.Awake();
        animator = GetComponent<Animator>();
    }

    private void FixedUpdate() {
        if ( isRunning && ( Time.time - runStartTime ) < runTimeDuration ) {
            Running();
        }
        else {
            FinishRun();
        }
    }

    public bool isRunning {
        private set => isUsed = value;
        get => animator.GetBool( TagsManager.ACTIVATE_PARAM );
    }

    override public void Use() {
        PutOnGround();
        StartRun();
    }

    private void PutOnGround() {
        DoBeforeDroppingStaff();
        transform.position = FindPositionOnGround();
    }

    private void StartRun() {
        isRunning = true;
        runStartTime = Time.time;
        animator.SetBool( TagsManager.ACTIVATE_PARAM, true );
        AudioManager.instance.mouseAudioSource.Play();
    }

    public void FinishRun() {
        isRunning = false;
        animator.SetBool( TagsManager.ACTIVATE_PARAM, false );
        AudioManager.instance.mouseAudioSource.Stop();
    }

    private void Running() {
        Vector3 target = transform.position + GetForwardDirection();      
        transform.position = Vector3.SmoothDamp( transform.position, target, ref velocity, runningSpeed );
        ChangeDirection();
    }

    private Vector3 GetForwardDirection() {
        if ( transform.localScale.x > 0 ) {
            return Vector3.right;
        }
        else {
            return Vector3.left;
        }
    }

    private void ChangeDirection() {
        Vector3 tempScale = transform.localScale;
        
        if ( transform.position.x < GameplayBorders.instance.leftBorder.transform.position.x ) {
            tempScale.x = Mathf.Abs( tempScale.x );
        }
        else if (transform.position.x > GameplayBorders.instance.rightBorder.transform.position.x ) {
            tempScale.x = -Mathf.Abs( tempScale.x );
        }

        transform.localScale = tempScale;
    }
}