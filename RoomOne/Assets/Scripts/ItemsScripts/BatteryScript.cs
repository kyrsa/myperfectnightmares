﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryScript : UsableItemController {
    override public void Use() {
        base.Use();
        GameObject flashlight = playerItemsHandler.GetItemInHand( TagsManager.FLASHLIGHT_TAG );

        if ( flashlight ) {
            flashlight.GetComponent<FlashlightScript>().intensity = 1f;
            isUsed = true;
            gameObject.SetActive( false );
        }
    }
}
