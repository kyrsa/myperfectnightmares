using UnityEngine;

public class KeyScript : UsableItemController {
    public GameObject myLight;
    public ToyBoxScript lockedItem;
    
    override public bool visible {
        set {
            base.visible = value;
            myLight.SetActive( value );
        }
        get => base.visible;
    }

    override public void Use() {
        if ( lockedItem.IsKeyInLock( gameObject ) ) {
            lockedItem.Open();
            AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.openDoorClip );
            Deactivate();
        }
        else {
            AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.itemLockedClip );
        }
    }

    private void Deactivate() {
        isUsed = true;
        gameObject.SetActive( false );
    }
}