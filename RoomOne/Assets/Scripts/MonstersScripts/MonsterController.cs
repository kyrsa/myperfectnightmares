using UnityEngine;

public class MonsterController : MonoBehaviour {
    protected BoxCollider2D myCollider;
    protected SpriteRenderer spriteRenderer;
    protected Animator animator;

    protected PlayerMovements victim;
    protected bool isHungry;
    protected bool isReachVictim;

    protected Vector3 homePoint;    
    public float huntingRadius = 5f;
    public float huntingSpeed = 0.1f;
    protected float leftStopBorder, rightStopBorder;

    virtual protected void Awake() {
        myCollider = GetComponent<BoxCollider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        
        isHungry = true;
        isReachVictim = false;
        homePoint = transform.position;      
    }

    virtual protected void Start() {
        victim = GameObject.FindGameObjectWithTag( TagsManager.PLAYER_TAG ).GetComponent<PlayerMovements>();
        SetStopFollowingBorders();
    }

    virtual protected void FixedUpdate() {
        if ( !GameplayController.instance.isFreezeMode ) {
            DoSomething();
        }
        else {
            StopMoving();
        }
    }

    virtual protected void DoSomething() {}
    
    virtual protected void SetStopFollowingBorders() {}

    virtual protected float leftHuntingBorder {get;} 

    virtual protected float rightHuntingBorder {get;} 

    virtual protected bool seeVictim {
        get => leftHuntingBorder < victim.transform.position.x && victim.transform.position.x < rightHuntingBorder && !victim.isHiding;
    }

    protected bool atHome {
        get => Mathf.Abs( transform.position.x - homePoint.x ) < 0.1f; 
    }

    virtual protected void OnTriggerEnter2D(Collider2D other) {
        if ( other.tag == TagsManager.PLAYER_HEALTH_TAG ) {
            isReachVictim = true;
        }
    }

    virtual protected void OnTriggerExit2D(Collider2D other) {
        if ( other.tag == TagsManager.PLAYER_HEALTH_TAG ) {
            isReachVictim = false;
        }
    }

    virtual protected void Hunt() {
        animator.SetTrigger( TagsManager.HUNT_PARAM );
        GoTo( victim.transform.position );
    }

    virtual protected void GoHome() {
        animator.SetTrigger( TagsManager.WALK_PARAM );
        GoTo( homePoint );
    }

    virtual protected void GoTo( Vector3 target ) {
        ChangeDirection( target );
        SetNewPosition( target );
    }

    virtual protected void ChangeDirection( Vector3 target ) {
        float direction = target.x < transform.position.x ? -1f : 1f;
        Vector3 tempScale = transform.localScale;
        tempScale.x = direction * Mathf.Abs( tempScale.x );
        transform.transform.localScale = tempScale;
    }

    virtual protected void SetNewPosition( Vector3 target ) {
        target.x = Mathf.Max( leftStopBorder, Mathf.Min( target.x, rightStopBorder ) );
        Vector3 smoothTarget = Vector3.MoveTowards( transform.position, target, huntingSpeed * Time.deltaTime );
        transform.position =  smoothTarget;
    }

    virtual protected void TryToKill() {
        if ( isReachVictim ) {
            victim.Killed();
        }
    }

    virtual protected void StopMoving() {
        animator.SetTrigger( TagsManager.WALK_PARAM );
    }
}