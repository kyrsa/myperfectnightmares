﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabaikaScript : MonsterController {
    public float underBedPosRange = 0.1f;
    private int underBedLayerOrder;
    public int outsideLayerOrder;

    float upperStopBorder, bottomStopBorder;
    public float lightHurtingIntensity = 0.8f;

    public CandyScript candy;
    public SecretBoxScript secretBox;

    override protected void Awake() {
        base.Awake();
        underBedLayerOrder = spriteRenderer.sortingOrder;
    }

    override protected void SetStopFollowingBorders() {
        leftStopBorder = transform.position.x - huntingRadius;
        rightStopBorder = transform.position.x + huntingRadius;

        upperStopBorder = transform.position.y;
        bottomStopBorder = GameplayBorders.instance.groundPosY;
    }

    override protected float leftHuntingBorder {
        get => leftStopBorder - myCollider.size.x / 2f;
    }
    
    override protected  float rightHuntingBorder {
        get => rightStopBorder + myCollider.size.x / 2f;
    }

    override protected void DoSomething() {
        if ( isHungry && seeVictim && !hasVictimBrightLight ) {
            Hunt();
            TryToKill();
        }
        else if ( seeCandy ) {
            TakeCandy();
        }
        else if ( !atHome ) {
            GoHome();
        }
    }

    private bool seeCandy {
        get => leftHuntingBorder < candy.transform.position.x && candy.transform.position.x < rightHuntingBorder && candy.GetComponent<CandyScript>().isOnFloor;
    }

    private bool hasVictimBrightLight {
        get {
            GameObject flashlight = victim.GetComponent<ItemsHandler>().GetItemInHand( TagsManager.FLASHLIGHT_TAG );
            return flashlight?.GetComponent<FlashlightScript>().intensity > lightHurtingIntensity;
        }
    }

    override protected void GoTo( Vector3 target ) {
        base.GoTo( target );
        SwitchUnderBedSprite();
    }

    override protected void SetNewPosition( Vector3 target ) {
        target.y = Mathf.Max( bottomStopBorder, Mathf.Min( target.y, upperStopBorder ) );
        base.SetNewPosition( target );
    }

    private void SwitchUnderBedSprite() {
        if ( Mathf.Abs( transform.position.y - upperStopBorder ) < underBedPosRange ) {
            spriteRenderer.sortingOrder = underBedLayerOrder;
        }
        else {
            spriteRenderer.sortingOrder = outsideLayerOrder;
        }
    }

    private void TakeCandy() {
        Vector3 oldPosition = transform.position;
        GoTo( candy.transform.position );

        if ( Vector3.Distance( transform.position, oldPosition ) < 0.001f ) {
            EatCandy();
            GivePresent();
        }
    }

    private void EatCandy() {
        candy.Eat();
        isHungry = false;
        AudioManager.instance.babaikaEatAudioSource.Play();
    }

    private void GivePresent() {
        secretBox.SetPosition( candy.transform.position );
    }
}
