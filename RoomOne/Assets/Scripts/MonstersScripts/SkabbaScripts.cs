using UnityEngine;

public class SkabbaScripts : MonsterController {
    public MouseScript mouse;

    override protected void SetStopFollowingBorders() {
        leftStopBorder = GameplayBorders.instance.leftBorder.transform.position.x;
        rightStopBorder = GameplayBorders.instance.rightBorder.transform.position.x;
    }

    override protected void DoSomething() {
        if ( isHungry && seeVictim ) {
            Hunt();
            TryToKill();
        }
        else if ( mouse.isRunning ) {
            FollowMouse();
        }
        else if ( !atHome ) {
            GoHome();
        }
    }

    override protected float leftHuntingBorder {
        get => transform.position.x - huntingRadius;
    }
    
    override protected  float rightHuntingBorder {
        get => transform.position.x + huntingRadius;
    }

    override protected void GoHome() {
        base.GoHome();

        if ( atHome ) {
            Wait();
        }
    }

    override protected void GoTo( Vector3 target ) {
        ChangeDirection( target );
        SetNewPosition( target );

        if ( !AudioManager.instance.skabbaMoveAudioSource.isPlaying ) {
            AudioManager.instance.skabbaMoveAudioSource.Play();
        }
    }

    override protected void SetNewPosition( Vector3 target ) {
        target.y = transform.position.y;
        target.z = transform.position.z;
        base.SetNewPosition( target );
    }

    private void Wait() {
        animator.SetTrigger( TagsManager.HOME_PARAM );
        transform.position = homePoint;
        ChangeDirection( transform.position + Vector3.left );
        AudioManager.instance.skabbaMoveAudioSource.Stop();
    }
    
    private void FollowMouse() {
        animator.SetTrigger( TagsManager.HUNT_PARAM );
        GoTo( mouse.transform.position );
    }
}