using UnityEngine;
using UnityEngine.UI;

public class OpenedBookScript : MonoBehaviour {
    [HideInInspector]
    public static OpenedBookScript instance;
    public Image cover;
    private UIBookScript openedBook;
    private int page;

    private void Awake() {
        MakeInstance();
    }

    private void Start() {
        gameObject.SetActive( false );
    }

    private void MakeInstance() {
        if ( instance == null ) {
            instance = this;
        }
        else {
            Destroy( gameObject );
        }
    }

    private bool visible {
        set {
            gameObject.SetActive( value );
        }
    }

    public void OpenBook( UIBookScript book ) {
        CloseBook();
        visible = true;
        openedBook = book;
        cover.sprite = openedBook.coverImg;
        page = 0;
        openedBook.OpenPage( page );
    }

    public void CloseBook() {
        if ( openedBook ) {
            openedBook.ClosePage( page );
        }

        visible = false;
    }

    public void NextPage() {
        if ( page < openedBook.sheets.Length - 1 ) {
            openedBook.ClosePage( page );
            openedBook.OpenPage( ++page );
            AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.bookFlipClip );
        }
    }

    public void PrevPage() {
        if ( page > 0 ) {
            openedBook.ClosePage( page );
            openedBook.OpenPage( --page );
            AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.bookFlipClip );
        }
    }
}