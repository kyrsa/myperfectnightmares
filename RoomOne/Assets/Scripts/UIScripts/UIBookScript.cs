﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBookScript : MonoBehaviour {
    public Sprite coverImg;
    public GameObject[] sheets;
    private Button button;


    private void Awake() {
        button = GetComponent<Button>();
        button.onClick.AddListener( OpenMe );
    }

    public void OpenMe() {
        OpenedBookScript.instance.OpenBook( this );
        AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.itemUsedClip );
    }    

    public void OpenPage( int page ) {
        sheets[page].SetActive( true );
    }

    public void ClosePage( int page ) {
        sheets[page].SetActive( false );
    }
}
