using UnityEngine;
using UnityEngine.UI;

public class IconScript : MonoBehaviour {
    private Image icon;
    public float iconDefSize;


    private void Awake() {
        icon = GetComponent<Image>();
    }

    public Sprite img {
        set {
            if ( value == null ) {
                alpha = 0f;
            }
            else {
                alpha = 1f;
                FitIconSize(value);
                icon.sprite = value;
            }
        }

        get => icon.sprite;
    }

    public float alpha {
        set {
            Color tempColor = icon.color;
            tempColor.a = value;
            icon.color = tempColor;
        }
    }

    private void FitIconSize( Sprite img ) {
        float scale = iconDefSize / Mathf.Max( img.bounds.size.x, img.bounds.size.y );
        icon.GetComponent<RectTransform>().sizeDelta = new Vector2( img.bounds.size.x * scale, img.bounds.size.y * scale );
    }
}