﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemsPanel : MonoBehaviour
{
    [HideInInspector]
    public static ItemsPanel instance;
    public IconScript handLeft;
    public IconScript handRight;
    public IconScript books;
    public IconScript bag;
    public GameObject itemIconPrefab;
    private IconScript[] icons;
    public float startPosX = 590f;
    public float iconsDistX = 110f;
    public GameObject booksPanel;

    private void Awake() {
        MakeInstance();
    }

    private void MakeInstance() {
        if ( instance == null ) {
            instance = this;
        }
        else {
            Destroy( gameObject );
        }
    }

    private void Start() {
        CreateIcons();
    }

    private void CreateIcons() { 
        int count = 10;
        icons = new IconScript[count];

        for ( int i = 0; i < count; i++ ) {
            icons[i] = Instantiate( itemIconPrefab, new Vector3(startPosX + i * iconsDistX, 0f, 0f), Quaternion.identity ).GetComponent<IconScript>();
            icons[i].transform.SetParent( transform, false );
        }
    }

    public void SetLeftHandItem ( Sprite img ) {
        handLeft.img = img;
    }

    public void SetRightHandItem ( Sprite img ) {
        handRight.img = img;
    }

    public void UpdateBagItems ( List<UsableItemController> items ) {
        for ( int i = 0; i < items.Count; i++ ) {
            icons[i].img = items[i].img;
        }

        for ( int i = items.Count; i < icons.Length; i++ ) {
            icons[i].img = null;
        }
    }

    public void ActivateBagIcon() {
        bag.alpha = 1f;
    }

    public void ActivateBooksIcon() {
        books.alpha = 1f;
        books.GetComponent<Button>().interactable = true;
    }

    public void ActivateBooksPanel() {
        if ( booksPanel.activeSelf ) {
            OpenedBookScript.instance.CloseBook();
            booksPanel.SetActive( false );
            GameplayController.instance.RequestUnfreezeMode( gameObject );
        }
        else if ( GameplayController.instance.RequestFreezeMode( gameObject) ) {
            booksPanel.SetActive( true );
        }
    }
}
