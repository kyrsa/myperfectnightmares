using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Text))]
public class LocalizationUI : MonoBehaviour {
    private Text textComponent;
    public string localizationKey;

    private void Awake() {
        textComponent = GetComponent<Text>();
        localizationKey = localizationKey.Trim();
    }

    private void OnEnable() {
        if ( !LocalizationSystem.isInitialized ) {
            LocalizationSystem.currentLanguage = LocalizationSystem.playerLanguage;
        }

        UpdateLocalization();
    }

    public void UpdateLocalization() {
        if ( !System.String.IsNullOrEmpty( localizationKey ) && LocalizationSystem.currentLocalization.ContainsKey( localizationKey ) ) {
            textComponent.text = LocalizationSystem.currentLocalization[localizationKey].Replace( @"\n", "" + '\n' );
        }
    }

    public string GetLocalized() {
        if ( LocalizationSystem.currentLocalization.ContainsKey( localizationKey ) ) {
            return LocalizationSystem.currentLocalization[localizationKey];
        }
        else {
            return string.Empty;
        }
    }

    public static void SetCurrentLanguage( SystemLanguage language ) {
        LocalizationSystem.currentLanguage = language;
        LocalizationUI[] allTextObjects = GameObject.FindObjectsOfType<LocalizationUI>();

        foreach ( var textObject in allTextObjects ) {
            textObject.UpdateLocalization();
        }
    }
}