﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovements : MonoBehaviour{
    Rigidbody2D body;
    Animator animator;
    public Transform groundPoint;

    public float moveForce = 5f;
    public float jumpForce = 5f;
    private bool onGround = true;
    private bool isAlive = true;

    private BoxCollider2D exitCollider;

    private void Awake() {
        body = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    private void Start() {
        exitCollider = GameplayBorders.instance.rightBorder.GetComponent<BoxCollider2D>();    
    }

    private void Update() {
        if ( !GameplayController.instance.isFreezeMode && !isHiding ) {
            Jump();
            CheckOnGroundStatus();
            Move();
            FinishLevel();
        }
        else if ( isAlive ){
            Freeze();
        }
    }

    private bool stepsSound {
        set {
            if ( value && !AudioManager.instance.playerAudioSource.isPlaying ){
                AudioManager.instance.playerAudioSource.Play();
            }
            else if ( !value ) {
                AudioManager.instance.playerAudioSource.Stop();
            }
        }
    }

    private void Move() {
        if ( onGround ) {
            float h = Input.GetAxisRaw( "Horizontal" );
            body.velocity = new Vector2( h * moveForce, body.velocity.y );
            animator.SetBool( TagsManager.WALK_PARAM, body.velocity.x != 0 );
            ChangeDirection(h);
            stepsSound = h != 0;
        }
    }

    private void ChangeDirection( float direction ) {
        if ( direction != 0 ) {
            Vector3 tempScale = transform.localScale;
            tempScale.x = Mathf.Sign(direction) * Mathf.Abs( tempScale.x );
            transform.localScale = tempScale;
        }
    }

    private void Jump() {
        bool isJumped = Input.GetAxisRaw( "Vertical" ) > 0 || Input.GetKeyDown( KeyCode.Space );

        if ( isJumped && onGround ) {
            onGround = false;
            animator.SetBool( TagsManager.JUMP_PARAM, true );
            body.velocity = new Vector2( body.velocity.x, jumpForce );
            stepsSound = false;
        }
    }

    private void CheckOnGroundStatus() {
        var hit = Physics2D.Raycast( groundPoint.position, Vector2.down, 0.1f, GameplayBorders.instance.groundLayer );

        if ( !onGround && hit ) {
            animator.SetBool( TagsManager.JUMP_PARAM, false );
            onGround = true;
        }
    }

    public void Freeze() {
        StopMoving();
        SetOnGround();
    }

    private void StopMoving() {
        body.velocity = new Vector2( 0f, 0f );
        animator.SetBool( TagsManager.WALK_PARAM, false );
        stepsSound = false;
    }

    private void SetOnGround(){
        if ( !onGround ) {
            var hit = Physics2D.Raycast( groundPoint.position, Vector2.down, 100f, GameplayBorders.instance.groundLayer );
            Vector3 newPosition = transform.position;
            newPosition.y -= hit.distance;
            transform.position = newPosition;
        }
    }

    public void Hide( float hidingPosX ) {
        StopMoving();
        SetOnGround();
        TeleportToPosition( hidingPosX );
        isHiding = true;
    }

    private void TeleportToPosition( float x ) {
        Vector3 newPosition = transform.position;
        newPosition.x = x;
        transform.position = newPosition;
    }

    public void GetOffHidingPlace() {
        isHiding = false;
    }

    public bool isHiding {
        private set => animator.SetBool( TagsManager.HIDE_PARAM, value );
        get => animator.GetBool( TagsManager.HIDE_PARAM );
    }

    public void Killed() {
        GameplayController.instance.PlayerKilledHandler();
        isAlive = false;
        SetOnGround();
        PlayDieAnimation();
        StartCoroutine( WaitDieAnimationAndStopGame() );
        AudioManager.instance.gameResultAudioSource.PlayOneShot( AudioManager.instance.gameOverClip );
    }

    private void PlayDieAnimation() {
        animator.SetTrigger( TagsManager.DIE_PARAM );
        body.velocity = new Vector2( 0f, jumpForce );
        onGround = false;
    }

    private IEnumerator WaitDieAnimationAndStopGame() {
        while ( !onGround ) {
            CheckOnGroundStatus();
            yield return new WaitForSeconds(1f);
        }

        GameplayController.instance.GameOver();
    }

    private bool canExit {
        get => GameObject.FindGameObjectWithTag( TagsManager.DOOR_TAG ).GetComponent<ItemController>().isUsed;
    }

    private bool isNearExit {
        get => groundPoint.GetComponent<BoxCollider2D>().IsTouching( exitCollider );
    }

    private void FinishLevel() {
        if ( isNearExit && canExit ) {
            GameplayController.instance.GameOver();
            AudioManager.instance.gameResultAudioSource.PlayOneShot( AudioManager.instance.levelUpClip );
        }
    }
}
