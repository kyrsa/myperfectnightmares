using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public struct Hand {
    public UsableItemController item {set; get;}
    public Transform holdingPoint { get;}
    public int itemLayerOrder {get;}
    public delegate void UpdateIconDelegate( Sprite sp );
    public UpdateIconDelegate UpdateIcon {get;}


    public Hand(Transform point, int layerOrder, UsableItemController target, UpdateIconDelegate func) {
        holdingPoint = point;
        itemLayerOrder = layerOrder;
        item = target;
        UpdateIcon = func;
    }
}

public class ItemsHandler : MonoBehaviour {
    // hands
    public Transform holdingPointL, holdingPointR;
    public int layerOrderL = 31, layerOrderR = 249;
    private Hand[] hands;

    // bag
    // [HideInInspector]
    public int bagSize = 0;

    // items
    [HideInInspector]
    public List<UsableItemController> items = new List<UsableItemController>();

    private void Start() {
        InitHands();
    }

    private void InitHands() {
        hands = new Hand[2];
        hands[0] = new Hand( holdingPointL, layerOrderL, null, ItemsPanel.instance.SetLeftHandItem );
        hands[1] = new Hand( holdingPointR, layerOrderR, null, ItemsPanel.instance.SetRightHandItem );
    } 

    void Update() {
        if ( !GameplayController.instance.isFreezeMode ) {
            SignalHandler();
        }
    }

    bool isRightHandSignal {
        get => Input.GetKey( KeyCode.LeftShift ) || Input.GetKey( KeyCode.RightShift );
    }

    void SignalHandler() {
        if ( Input.GetKeyDown( KeyCode.E ) && isRightHandSignal ) {
            Use(1);
        } 
        else if ( Input.GetKeyDown( KeyCode.E ) ) {
            Use(0);
        }

        if ( Input.GetKeyDown( KeyCode.R ) && isRightHandSignal ) {
            Drop(1);
        }
        else if ( Input.GetKeyDown( KeyCode.R ) ) {
            Drop(0);
        } 

        if ( Input.GetKeyDown( KeyCode.Q ) ) {
            SwitchItems();
        }
    }

    public void Grap( UsableItemController target ) {
        int handIndex = target.tag == TagsManager.FLASHLIGHT_TAG ? 1 : 0;
        
        if ( hands[handIndex].item != null ) {
            TryToHoldInHand( 1 - handIndex, hands[handIndex].item );
        }

        HoldInHand( handIndex, target );
    }

    private void TryToHoldInHand( int handIndex, UsableItemController target ) {
        if ( hands[handIndex].item == null ) {
            HoldInHand( handIndex, target );
        }
        else {
            TryToPutInBag( target );
        }
    }

    private void HoldInHand( int handIndex, UsableItemController target ) {
        UpdateHandItem( handIndex, target );
        target.HoldInHand( hands[handIndex].holdingPoint, hands[handIndex].itemLayerOrder );
    }

    private void UpdateHandItem( int handIndex, UsableItemController target ) {
        hands[handIndex].item = target;
        hands[handIndex].UpdateIcon( target?.img );
    } 

    private void TryToPutInBag( UsableItemController target ) {
        if ( bagSize >= target.weight ) {
            PutInBag( target );            
        }
        else {
            target.Drop();
        }
    }

    private void PutInBag( UsableItemController target ) {
        bagSize -= target.weight;
        items.Add(target);
        target.gameObject.SetActive( false );
        ItemsPanel.instance.UpdateBagItems( items );
    } 

    private void Use( int handIndex ) {
        if ( hands[handIndex].item != null ) {
            hands[handIndex].item.Use();
        
            if ( hands[handIndex].item.isUsed ) {
                UpdateHandItem( handIndex, null );
            }
        }
    }

    private void Drop( int handIndex ) {
        if ( hands[handIndex].item != null ) {
            hands[handIndex].item.Drop();
            UpdateHandItem( handIndex, null );
        }
    }

    private void SwitchItems() {
        if ( items.Count > 0 ) {
            var oldItem = hands[0].item;
            var newItem = GetItemOutOfBag(0);
            HoldInHand( 0, newItem );

            if ( oldItem ) {
                TryToHoldInHand( 1, oldItem );      
            }

            AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.itemUsedClip );
        }
        else {
            AudioManager.instance.itemAudioSource.PlayOneShot( AudioManager.instance.itemLockedClip );
        }
    }

    private UsableItemController GetItemOutOfBag( int index ) {
        var item = items[index];
        items.RemoveAt( index );
        bagSize += item.weight; 
        item.gameObject.SetActive( true );
        ItemsPanel.instance.UpdateBagItems( items );

        return item;
    }

    public void FindBag( int size ) {
        bagSize = size;
        ItemsPanel.instance.ActivateBagIcon();
    }

    public GameObject GetItemInHand( string tag ) {
        foreach ( var hand in hands ) {
            if ( hand.item?.tag == tag ) {
                return hand.item.gameObject;
            }
        }

        return null;
    }
}