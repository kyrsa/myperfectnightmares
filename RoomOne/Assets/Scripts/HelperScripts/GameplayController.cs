using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.SceneManagement;


public enum GameMode {
    Help,
    General
}


public class GameplayController : MonoBehaviour {
    private bool isPaused = false;
    private bool isGameOver = false;

    public GameObject pausePanel;
    public GameObject gameOverPanel;

    [HideInInspector]
    public static GameplayController instance;  
    private GameObject freezingObject;
    public GameMode gameMode = GameMode.Help; 

    public GameObject globalLight;
    public float globalLightIntensity = 0.1f;

    public Transform itemsParent;

    private void Awake() {
        MakeInstance();   
    }

    private void MakeInstance() {
        if ( instance == null ) {
            instance = this;
        }
        else {
            Destroy( gameObject );
        }
    }

    private void OnEnable() {
        globalLight.GetComponent<Light2D>().intensity = globalLightIntensity;
    }
    
    public bool isFreezeMode {
        get => freezingObject != null || isPaused || isGameOver;
    }

    public bool RequestFreezeMode( GameObject sender ) {
        if ( isFreezeMode ) {
            return false;
        }
        else {
            freezingObject = sender;
            return true;
        }
    }

    public bool RequestUnfreezeMode( GameObject sender ) {
        if ( freezingObject == sender ) {
            freezingObject = null;
            return true;
        }
        else {
            return false;
        }
    }

    public void Pause() {
        if ( !isGameOver ) {
            isPaused = true;
            Time.timeScale = 0f;
            pausePanel.SetActive( true );
        }
    }

    public void Resume() {
        isPaused = false;
        Time.timeScale = 1f;
        pausePanel.SetActive( false );
    }

    public void PlayerKilledHandler() {
        isGameOver = true;
    }

    public void GameOver() {
        Time.timeScale = 0f;
        gameOverPanel.SetActive( true );
    }

    public void Restart() {
        Time.timeScale = 1f;
        Destroy( gameObject);
        SceneManager.LoadScene( SceneManager.GetActiveScene().name );
    }

    public void OpenMainMenu() {
        Time.timeScale = 1f;
        SceneManager.LoadScene( TagsManager.MAIN_MENU_SCENE );
    }
}