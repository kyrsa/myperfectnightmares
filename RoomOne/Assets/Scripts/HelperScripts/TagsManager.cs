public static class TagsManager {
    // SCENES
    public const string ROOM_ONE_SCENE = "RoomOne";
    public const string ROOM_ONE_HELP_MODE_SCENE = "RoomOneHelpMode";
    public const string MAIN_MENU_SCENE = "MainMenu";
    public const string INTRO_CUTSCENE = "IntroCutscene";

    // SETTINGS KEYS
    public const string GAMEMODE_KEY = "GameMode";
    public const string LOCALIZATION_KEY = "Localization";

    // LAYERS
    public const string PLAYER_LAYER = "Player";
    public const string BEHIND_LAYER = "Behind";
    public const string AHEAD_LAYER = "Ahead";

    // TAGS
    public const string ROOM_LEFT_BORDER_TAG = "RoomLeftBorder";
    public const string ROOM_RIGHT_BORDER_TAG = "RoomRightBorder";
    public const string PLAYER_TAG = "Player";
    public const string PLAYER_HEALTH_TAG = "PlayerHealth";
    public const string FLASHLIGHT_TAG = "Flashlight";
    public const string BAG_TAG = "Bag";
    public const string WATER_TAG = "Water";
    public const string TOY_BOX_TAG = "ToyBox";
    public const string DOOR_TAG = "Door";
    public const string AIMER_TARGET = "AimerTarget";    


    // ANIMATION TAGS
    public const string WALK_PARAM = "Walk";
    public const string JUMP_PARAM = "Jump";
    public const string HIDE_PARAM = "Hide";
    public const string DIE_PARAM = "Die";
    public const string ACTIVATE_PARAM = "Activate";
    public const string HIGHLIGHT_PARAM = "Highlight";
    public const string HUNT_PARAM = "Hunt";
    public const string HOME_PARAM = "Home";
}