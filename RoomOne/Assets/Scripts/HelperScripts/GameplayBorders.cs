using UnityEngine;

public class GameplayBorders : MonoBehaviour {
    public static GameplayBorders instance;
    public Transform leftBorder;
    public Transform rightBorder;
    public Transform ground;
    public LayerMask groundLayer;

    private void Awake() {
        MakeInstance();   
    }

    private void MakeInstance() {
        if ( instance == null ) {
            instance = this;
        }
        else {
            Destroy( gameObject );
        }
    }

    public float groundPosY {
        get => ground.position.y + ground.GetComponent<BoxCollider2D>().size.y / 2f;
    }

}