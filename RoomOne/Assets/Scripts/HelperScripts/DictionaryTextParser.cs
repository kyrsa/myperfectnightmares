using UnityEngine;
using System.Collections.Generic;

public class DictionaryTextParser : MonoBehaviour {
    private static string [] lineSeparator = new string[] {"\n", "\n\r", "\r\n"};
    private static string [] pairSeparator = new string[] {"\t", " "};

    public static Dictionary<string, string> Parse( string text, string[] lineSeparator, string[] pairSeparator ) {
        Dictionary<string, string> dictionary = new Dictionary<string, string>();
        string[] lines = text.Split( lineSeparator, System.StringSplitOptions.RemoveEmptyEntries );

        foreach ( var line in lines ) {
            string[] pair = line.Trim().Split( pairSeparator, 2, System.StringSplitOptions.None );

            if ( pair.Length == 2 ) {
                dictionary.Add( pair[0].Trim(), pair[1].Trim() );
            }
        }

        return dictionary;
    }

    public static Dictionary<string, string> Parse( string text ) {
        return Parse( text, lineSeparator, pairSeparator );
    }
}