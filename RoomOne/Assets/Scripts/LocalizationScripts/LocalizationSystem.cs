using System.Collections.Generic;
using UnityEngine;

public class LocalizationSystem {
    public static SystemLanguage[] avialableLanguages = new SystemLanguage[2] { SystemLanguage.English, SystemLanguage.Russian };
    private static SystemLanguage _currentLanguage;
    public static Dictionary<string, string> currentLocalization = new Dictionary<string, string>();
    private static string fileFolder = "Languages/";
    private static TextAsset currentLocalizationFile;
    private static bool _isInitialized = false;


    public static bool isInitialized {
        get => _isInitialized;
    } 

    public static SystemLanguage currentLanguage {
        get => _currentLanguage;

        set {
            _currentLanguage = value;
            LoadLanguage( value );

            if ( currentLocalizationFile == null ) {
                Debug.LogWarningFormat( "Missing localization file for {0} language, loading {1}", _currentLanguage, avialableLanguages[0] );
                LoadLanguage( avialableLanguages[0] );
            }

            currentLocalization = DictionaryTextParser.Parse( currentLocalizationFile.text );
            _isInitialized = true;
        }
    }

    private static void LoadLanguage( SystemLanguage language ) {
        _currentLanguage = language;
        currentLocalizationFile = Resources.Load<TextAsset>( fileFolder + _currentLanguage.ToString().ToLower() );
    }

    public static SystemLanguage playerLanguage {
        get => (SystemLanguage)PlayerPrefs.GetInt( TagsManager.LOCALIZATION_KEY, (int)avialableLanguages[0] );
    }
}