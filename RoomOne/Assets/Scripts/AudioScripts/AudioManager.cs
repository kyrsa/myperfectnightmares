using UnityEngine;

public class AudioManager : MonoBehaviour {
    public static AudioManager instance;

    public AudioSource itemAudioSource;
    public AudioClip itemLockedClip;
    public AudioClip itemSelectedClip;
    public AudioClip itemDroppedClip;
    public AudioClip itemUsedClip;
    public AudioClip openDoorClip;
    public AudioClip bookFlipClip;
    public AudioClip starSelectedClip;
    
    public AudioSource waterAudioSource;
    public AudioSource mouseAudioSource;
    public AudioSource aimerAudioSource;

    public AudioSource babaikaEatAudioSource;
    public AudioSource skabbaMoveAudioSource;

    public AudioSource playerAudioSource;
    public AudioSource gameResultAudioSource;
    public AudioClip gameOverClip;
    public AudioClip levelUpClip;

    private void Awake() {
        MakeInstance();   
    }

    private void MakeInstance() {
        if ( instance == null ) {
            instance = this;
        }
        else {
            Destroy( gameObject );
        }
    }
}