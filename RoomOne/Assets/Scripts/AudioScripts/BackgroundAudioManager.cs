using UnityEngine;

public class BackgroundAudioManager : MonoBehaviour {
    public static BackgroundAudioManager instance;
    public AudioSource backgroundAudioSource;
    public float soundVolume = 0.1f;

    private void Awake() {
        MakeInstance();   
    }

    private void MakeInstance() {
        if ( instance == null ) {
            instance = this;
            DontDestroyOnLoad( gameObject );
        }
        else {
            Destroy( gameObject );
        }
    }

    public bool sound {
        set => instance.backgroundAudioSource.volume = value ? soundVolume : 0f;
        get => instance.backgroundAudioSource.volume > 0f;
    }
}